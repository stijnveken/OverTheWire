## level7 -> level8

### Solution
The secret we need to enter in the form in encoded in the source code. 

```php
$encodedSecret = "3d3d516343746d4d6d6c315669563362";

function encodeSecret($secret) {
    return bin2hex(strrev(base64_encode($secret)));
}
```

We just need to reverse the encoding

```php
echo base64_decode(strrev(hex2bin($encodedSecret)));
```
### password
`W0mMhUcRRnG8dcghE4qvk3JA9lGt8nDl`