## Level23 -> Level24

**Level Goal**
A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.

NOTE: This level requires you to create your own first shell-script. This is a very big step and you should be proud of yourself when you beat this level!

NOTE 2: Keep in mind that your shell script is removed once executed, so you may want to keep a copy around…

**Answer**
Script that the cronjob executes

```bash 
#!/bin/bash

myname=$(whoami)

cd /var/spool/$myname
echo "Executing and deleting all scripts in /var/spool/$myname:"
for i in * .*;
do
    if [ "$i" != "." -a "$i" != ".." ];
    then
	echo "Handling $i"
	timeout -s 9 60 ./$i
	rm -f ./$i
    fi
done
```
We need to write a script that moves the password from the password file to a file we can read.

Our script, don't forget to make it executable and get all the persmissions in order

```bash
cat /etc/bandit_pass/bandit24 > /tmp/fake123/pass
```

**Password**
`UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ`