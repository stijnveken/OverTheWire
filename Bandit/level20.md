## Level20 -> Level21

**Level Goal**
There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).


**Answer**
Echo the password of the current level to a nc server on a port. Let the binary connect to that port. It will read the password and if it is correct, echo the new one.


**Password**
`gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr`
