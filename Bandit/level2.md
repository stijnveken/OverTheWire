## Level2 -> Level3

**Level Goal**

The password for the next level is stored in a file called spaces in this filename located in the home directory

**Answer**

`cat ./spaces\ in\ this\ filename`

**Password**

`UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK`