## Level28 -> Level29

**Level Goal**
There is a git repository at ssh://bandit28-git@localhost/home/bandit28-git/repo. The password for the user bandit28-git is the same as for the user bandit28.

Clone the repository and find the password for the next level.

**Answer**


**Password**
`bbc96594b4e001778eee9975372716b2`
