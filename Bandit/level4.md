## Level4 -> Level5

**Level Goal**

The password for the next level is stored in the only human-readable file in the inhere directory.

**Answer**

`file ./inhere/*`

`cat ./inhere/-file07`

**Password**

`koReBOKuIDDepwhWk7jZC0RTdopnAYKh`