## Level10 -> Level11

**Level Goal**

The password for the next level is stored in the file data.txt, which contains base64 encoded data

**Answer**

`cat data.txt | base64 --decode`

**Password**

`IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR`