## Level15 -> Level16

**Level Goal**

The password for the next level can be retrieved by submitting the password of the current level to port 30001 on localhost using SSL encryption.

man
**Helpful note: Getting “HEARTBEATING” and “Read R BLOCK”? Use -ign_eof and read the “CONNECTED COMMANDS” section in the manpage. Next to ‘R’ and ‘Q’, the ‘B’ command also works in this version of that command…**


**Answer**

`openssl s_client -connect localhost:30001 -ign_eof`

`BfMYroe26WYalil77FoDi9qh59eK5xNr`


**Password**

`cluFn7wTiGryunymYOu4RcffSxQluehd`