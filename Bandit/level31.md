## Level31 -> Level32

**Level Goal**
There is a git repository at ssh://bandit30-git@localhost/home/bandit30-git/repo. The password for the user bandit30-git is the same as for the user bandit30.

**Answer**
Add a `key.txt` file with the contents `May I come in?`. Remove the gitignore
**Password**
`56a9bf19c63d650ce78e6ec0354ee45e`
