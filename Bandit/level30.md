## Level30 -> Level31

**Level Goal**
There is a git repository at ssh://bandit30-git@localhost/home/bandit30-git/repo. The password for the user bandit30-git is the same as for the user bandit30.

**Answer**
git show secret

**Password**
`47e603bb428404d265f59c42920d81e5`
