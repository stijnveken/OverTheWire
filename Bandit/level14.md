## Level14 -> Level15

**Level Goal**

The password for the next level can be retrieved by submitting the password of the current level to **port 30000 on localhost.**


**Answer**

`nc localhost 300000`

`4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e`


**Password**

`BfMYroe26WYalil77FoDi9qh59eK5xNr`