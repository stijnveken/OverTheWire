## Level22 -> Level23

**Level Goal**
A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.

NOTE: Looking at shell scripts written by other people is a very useful skill. The script for this level is intentionally made easy to read. If you are having problems understanding what it does, try executing it to see the debug information it prints.


**Answer**
This cronjob copies the password from one file to another based on the md5 hash of the username.

```bash
#!/bin/bash

myname=$(whoami) # bandit23
# md5sum = 8ca319486bfbbc3663ea0fbe81326349
mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)

# The password is in the file 8ca319486bfbbc3663ea0fbe81326349
echo "Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget"

cat /etc/bandit_pass/$myname > /tmp/$mytarget

```

cat /tmp/8ca319486bfbbc3663ea0fbe81326349

**Password**
`jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n`