## Level25 -> Level26

**Level Goal**
Logging in to bandit26 from bandit25 should be fairly easy… The shell for user bandit26 is not /bin/bash, but something else. Find out what it is, how it works and how to break out of it.

**Answer**
the shell for bandit26 executes more on a textfile. Making the screen small enough that it doesn't fit on screen we can interupt the flow and open the password file with vi

**Password**
`5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z`