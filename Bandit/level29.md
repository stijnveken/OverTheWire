## Level29 -> Level30

**Level Goal**
There is a git repository at ssh://bandit29-git@localhost/home/bandit29-git/repo. The password for the user bandit29-git is the same as for the user bandit29.

**Answer**
git checkout dev

**Password**
`5b90576bedb2cc04c86a9e924ce42faf`
