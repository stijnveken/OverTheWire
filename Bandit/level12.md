## Level12 -> Level13

**Level Goal**

The password for the next level is stored in the file data.txt, which is a hexdump of a file that has been repeatedly compressed. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!)

**Answer**

`mkdir /tmp/your-name-here`

`cp data.txt /tmp/your-name-here`

`xxd -r data.txt data`

`file data`

`mv data data.gz`

`gzip -d data.gz`

`file data`

`mv data data.bz2`

`bunzip2 data.bz2`

`file data`

`mv data data.gz`

`gzip -d data.gz`

`file data`

`mv data data.tar`

`tar -xvf data.tar`

`file data5.bin`

`mv data5.bin data.tar`

`tar -xvf data.tar`

`file data6.bin`

`mv data6.bin data.bz2`

`bunzip2 data.bz2`

`file data`

`mv data data2.tar`

`tar -xvf data2.tar`

`file data8.bin`

`mv data8.bin data.gz`

`gunzip data.gz`

`file data`

`cat data`

**Password**

`8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL`
