## Level5 -> Level6

**Level Goal**

The password for the next level is stored in a file somewhere under the inhere directory and has all of the following properties: - human-readable - 1033 bytes in size - not executable

**Answer**

`cd inhere`

`find . -size 1033c`

`cat ./maybehere07/.file2`

**Password**

`DXjZPULLxYr17uwoI01bNLQbtFemEgo7`