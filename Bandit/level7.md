## Level7 -> Level8

**Level Goal**

The password for the next level is stored in the file data.txt next to the word millionth

**Answer**

`grep data.txt -e "millionth"`

**Password**

`cvX2JJa4CFALtqS87jk27qwqGhBM9plV`