## Level16 -> Level17

**Level Goal**

The credentials for the next level can be retrieved by submitting the password of the current level to **a port on localhost in the range 31000 to 32000.** First find out which of these ports have a server listening on them. Then find out which of those speak SSL and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.


**Answer**

`nmap -p 31000:320000 localhost`

`nmap -sV --version-light -p 31790 localhost`

`Save the private key to a local file`

`chmod 600 keyfile`

`ssh -i keyfile bandit17@localhost`

`cat /etc/bandit_pass/bandit17`

**Password**

`xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn`