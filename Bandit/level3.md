## Level3 -> Level4

**Level Goal**

The password for the next level is stored in a hidden file in the inhere directory.

**Answer**

`cat ./inhere/.hidden`

**Password**

`pIwrPrtPN36QITSp3EQaw936yaFoFgAB`