## Level27 -> Level28

**Level Goal**
There is a git repository at ssh://bandit27-git@localhost/home/bandit27-git/repo. The password for the user bandit27-git is the same as for the user bandit27.

Clone the repository and find the password for the next level.

**Answer**
simply clone the repo and cat the readme

**Password**
`0ef186ac70e04ea33b4c1853d2526fa2`
