## Level26 -> Level27

**Level Goal**
Good job getting a shell! Now hurry and grab the password for bandit27!

**Answer**
Escape from the shell https://burakb.net/walkthroughs/overthewire-bandit-level-25-level-26/ and quickly grab the password with bandit27-do

./bandit27-do cat /etc/bandit_pass/bandit27

**Password**
`3ba3118a22e93127a4ed485be72ef5ea`
